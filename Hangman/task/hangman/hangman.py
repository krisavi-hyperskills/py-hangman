import random
import string
title = "HANGMAN"
word_list = ['python', 'java', 'kotlin', 'javascript']
word = random.choice(word_list)
guessed = list()
print(f"{' '.join(title)}")
while True:
    start = input('Type "play" to play the game, "exit" to quit: ')
    if start == 'exit':
        break
    elif start == 'play':
        result = "You are hanged!"
        lives = 8
        while lives != 0:
            remaining = [letter if letter in guessed else '-' for letter in word]
            if '-' not in remaining:
                print("You guessed the word!")
                result = "You survived!"
                break
            print()
            print(''.join(remaining))
            guess = input("Input a letter: ")
            if len(guess) != 1:
                print("You should print a single letter")
                continue
            elif guess not in string.ascii_lowercase:
                print("It is not an ASCII lowercase letter")
                continue
            if guess in guessed:
                print("You already typed this letter")
                # lives -= 1
            elif guess not in word:
                print("No such letter in the word")
                lives -= 1
            guessed.append(guess)
        print(f"{result}")
